package com.ADSI1836648.Bike.service;

import com.ADSI1836648.Bike.domain.Client;
import com.ADSI1836648.Bike.domain.Sale;
import com.ADSI1836648.Bike.service.dto.ClientWithSaleDTO;
import com.ADSI1836648.Bike.service.dto.SaleDTO;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IClientService {

    public Iterable<Client> read(String document, String email);

    public Client findClientByDocumentContains(String document);

    public Client update(Client client);

    public ResponseEntity<Client> create(Client client);

    public List<Sale> findClientByDocumentWithSale(String document);

    public void delete(Integer id);

    public Iterable<Client> search(String documentNumber);
}
