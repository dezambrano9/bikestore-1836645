package com.ADSI1836648.Bike.service.dto;

import com.ADSI1836648.Bike.domain.enumeration.TypeShockAbsorber;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
public class BikeDTO implements Serializable {

    @NotNull
    private int id;

    private String model;
    private Double price;
    private String serial;
    private Boolean status;
    private TypeShockAbsorber typeShockAbsorber;

}
