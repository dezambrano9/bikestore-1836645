package com.ADSI1836648.Bike.service;

import com.ADSI1836648.Bike.domain.Users;
import com.ADSI1836648.Bike.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IUserServiceImp implements IUserService{

    @Autowired
    UserRepository userRepository;

    @Override
    public Users save(Users user) {
        return userRepository.save(user);
    }

    @Override
    public Iterable<Users> getAll() {
        return userRepository.findAll();
    }
}
